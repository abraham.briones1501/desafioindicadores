/*
Crear una nueva app que me permita hacer seguimiento de los valores de indicadores monetarios.
Menu
1. Actualizar datos(Escribe nuevo archivo).
2.- Promedio ultimos 5 archivos
3.- Minimo y maximo ultimos 5 archivos.
4.- Salir

Observaciones
- Utilice la API mi indicador (mindicador.cl)
- Haga uso de promesas
- La persistencia de la aplicacion debe ser en archivos JSON.
- Divida la aplicacion en los siguientes modulos :
    DataAPI
    FilesAPI
*/
const readline = require('readline');
var actualizarFuncion = require('./librerias/dataAPI.js');

var lector = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

function menu() {
	console.log('Qué desea hacer?:');
	console.log('1.- Actualizar datos');
	console.log('2.- Promedio últimos 5 archivos');
	console.log('3.- Minimo y maximo últimos 5 archivos');
	console.log('4.- Salir');
	lector.question('Escriba su opcion: ', opcion => {
		switch (opcion) {
			case '1':
				console.log('1');
				actualizarFuncion();
				break;
			case '2':
				console.log('2');
				console.log(Date.now());
				break;
			case '3':
				console.log('3');
				break;
			case '4':
				console.log('Adios!');
				lector.close();
				process.exit(0);
				break;
			default:
				console.log('Opción incorrecta, intenta de nuevo');
				menuFuncion(lector, menuCallBack);
				break;
		}
	});
}

menu();



